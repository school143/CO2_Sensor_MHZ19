#include "CO2_Sensor_MHZ19.h"

CO2_Sensor_MHZ19 co2;

void setup() {
    Serial.begin(9600); 
    co2 = CO2_Sensor_MHZ19(10);
}

void loop(){
  double ppm = co2.GetCO2Concentration();
  Serial.println(ppm);
  Serial.println("-----------");
  delay(1000);
}
