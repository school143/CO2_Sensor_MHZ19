class CO2_Sensor_MHZ19 {
public:
	CO2_Sensor_MHZ19(int analogPin);
	double GetCO2Concentration();
private:
	const int timeout = 100000;
	int pinId;
};