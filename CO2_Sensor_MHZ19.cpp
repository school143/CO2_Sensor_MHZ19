CO2_Sensor_MHZ19::CO2_Sensor_MHZ19(int analogPin) {
	pinId = analogPin;
	pinMode(pinId, INPUT);
}

double CO2_Sensor_MHZ19::GetCO2Concentration() {
	double h, l, ppm = 0;
	int to = timeout;
	do {
		h = pulseIn(pinId, HIGH, timeout) / 1000.0;
		l = 1004 - h;
		ppm = 2000 * (h-2)/(h+l-4);
	} while (ppm < 0.0 && to--);
	return ppm;
}